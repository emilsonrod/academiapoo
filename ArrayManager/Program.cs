﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayManager
{
    class Program
    {
        static void Main(string[] args)
        {
            GeneradorArray generador = new GeneradorArray();
            EvaluadorArray evaluador = new EvaluadorArray();
            generador.GenerarNumeros(100);
            //generador.GenerarNumeros(10);
            List<int> result = evaluador.EvaluarLista(generador.ListaNumeros, 7);
        }
    }
}
