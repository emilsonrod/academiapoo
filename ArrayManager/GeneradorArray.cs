﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayManager
{
    class GeneradorArray
    {
        private List<int> listaNumeros;
        private Random random;
        public GeneradorArray()
        {
            listaNumeros = new List<int>();
            random = new Random();
        }

        public List<int> ListaNumeros
        {
            get
            {
                return listaNumeros;
            }
        }

        public void GenerarNumeros(int tamanio)
        {
            listaNumeros.Clear();
            for (int count = 1; count <= tamanio; count++)
            {
                listaNumeros.Add(random.Next(-tamanio,tamanio));
            }
        }
    }
}
