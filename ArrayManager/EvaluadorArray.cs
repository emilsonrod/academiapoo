﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayManager
{
    class EvaluadorArray
    {
        public List<int> EvaluarLista(List<int> listaActual, int parametro)
        {
            List<int> listaPares = new List<int>();
            foreach (int numero in listaActual)
            {
                if (numero % parametro == 0)
                    listaPares.Add(numero);
            }
            return listaPares;
        }
    }
}
