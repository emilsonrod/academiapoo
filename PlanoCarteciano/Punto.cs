﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanoCarteciano
{
    class Punto
    {
        public int CoordX { get; set; }
        public int CoordY { get; set; }

        public double CalcularDistanciaOrigen()
        {
            return Math.Sqrt((Math.Pow(CoordX, 2))+(Math.Pow(CoordY,2)));
        }
    }
}
