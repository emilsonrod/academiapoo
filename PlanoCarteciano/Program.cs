﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanoCarteciano
{
    class Program
    {
        static void Main(string[] args)
        {
            ManagerPoints manager = new ManagerPoints();
            manager.GenrarPuntos(10);
            manager.ImprimirDistancias();
            Console.WriteLine(manager.MayorDistancia.CalcularDistanciaOrigen());
        }
    }
}
