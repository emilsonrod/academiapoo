﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanoCarteciano
{
    class ManagerPoints
    {
        private List<Punto> listaPunto;
        private Punto mayorDistancia;
        private Random random;
        private const int limiteCarteciano = 50;

        public ManagerPoints()
        {
            listaPunto = new List<Punto>();
            mayorDistancia = new Punto();
            random = new Random();
        }

        public List<Punto> ListaPunto
        {
            get
            {
                return listaPunto;
            }
        }

        internal Punto MayorDistancia
        {
            get
            {
                return mayorDistancia;
            }
        }

        public void AgregarPunto(Punto punto)
        {
            if (punto.CalcularDistanciaOrigen() >
                mayorDistancia.CalcularDistanciaOrigen())
                mayorDistancia = punto;
            listaPunto.Add(punto);
        }

        public void GenrarPuntos(int cantidad)
        {
            for (int count = 1; count <= cantidad; count++)
            {
                Punto punto = new Punto();
                punto.CoordX = random.Next(-limiteCarteciano, limiteCarteciano);
                punto.CoordY = random.Next(-limiteCarteciano, limiteCarteciano);
                AgregarPunto(punto);
            }
        }

        public void ImprimirDistancias()
        {
            foreach (Punto punto in listaPunto)
            {
                Console.WriteLine(punto.CalcularDistanciaOrigen());
            }
        }
    }
}
