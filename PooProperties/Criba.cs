﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PooProperties
{
    class Criba
    {
        int[] criba;
        public void GenerarCriba(int limite)
        {
            criba = new int[limite - 1];
            for (int index = 0; index < limite-1; index++)
            {
                criba[index] = index + 2;
            }
        }

        public void AplicarAlgoritmo()
        {
            for (int index = 0; index < criba.Length-1; index++)
            {
                if (criba[index] == 0)
                    continue;
                for (int index2 = index + 1; index2 < criba.Length - 1; index2++)
                {                    
                    if (criba[index2] != 0 && 
                        criba[index2] % criba[index] == 0)
                    {
                        criba[index2] = 0;
                    }
                }
            }
        }

        public void Imprimir()
        {
            foreach (int numero in criba)
            {
                if(numero != 0)
                    Console.WriteLine(numero);
            }
        }
    }
}
