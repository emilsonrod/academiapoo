﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PooProperties
{
    class Planilla
    {
        Estudiante[] listaEstudiantes;
        List<Estudiante> listaDinamicaEstudiantes;

        public Planilla()
        {
            listaEstudiantes = new Estudiante[15];
            listaDinamicaEstudiantes = new List<Estudiante>(); 
        }

        internal Estudiante[] ListaEstudiantes
        {
            get
            {
                return listaEstudiantes;
            }

            set
            {
                listaEstudiantes = value;
            }
        }

        public void AgregarEstudiante(Estudiante estudiante)
        {
            int currentIndex = GetEmptyIndex();
            if (currentIndex > -1)
                ListaEstudiantes[currentIndex] = estudiante;
            else
                Console.WriteLine("Lista llena");

            if (listaDinamicaEstudiantes.Count < 15)
            {
                listaDinamicaEstudiantes.Add(estudiante);
                listaDinamicaEstudiantes.Where(es => es.NotaFinal > 30);
            }
        }

        public void AgregarEstudiante(string nombre, int edad, 
            string ci, string codigo)
        {
            Estudiante estudiante = new Estudiante(nombre,edad,
                codigo,ci);
            AgregarEstudiante(estudiante);
        }

        public int GetEmptyIndex()
        {
            for (int index=0;index< ListaEstudiantes.Length; index++)
            {
                if (ListaEstudiantes[index] == null)
                    return index;
            }
            return -1;
        }
    }
}
