﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PooProperties
{
    class Estudiante
    {
        private string nombre;
        private int edad;
        private string codigoEstudiante;
        private string ci;
        private int notaFinal;

        //sobreescribiendo constructor por defecto
        public Estudiante()
        {
            nombre = "Blanco";
            edad = -1;
            codigoEstudiante = "000000";
            ci = "00000000";
        }

        //Sobrecarga de constructor
        public Estudiante(string nombre, int edad,
            string codigoEstudiante, string ci)
        {
            Nombre = nombre;
            this.ci = ci;
            this.codigoEstudiante = codigoEstudiante;
            this.edad = edad;
        }

        public Estudiante(string nombre, string ci)
        {
            this.nombre = nombre;
            this.ci = ci;
            codigoEstudiante = string.Empty;
            edad = -1;
        }

        public string Nombre
        {
            get
            {
                if (Edad > 17)
                    return nombre;
                else
                    return "es menor de edad";
            }
            set
            {
                nombre = value;
            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }

            set
            {
                edad = value;
            }
        }

        public string CodigoEstudiante
        {
            get
            {
                return codigoEstudiante;
            }

            set
            {
                codigoEstudiante = value;
            }
        }

        public string Ci
        {
            get
            {
                return ci;
            }

            set
            {
                ci = value;
            }
        }

        public int NotaFinal
        {
            get
            {
                return notaFinal;
            }

            set
            {
                notaFinal = value;
            }
        }

        public void Presentarse()
        {
            Console.WriteLine("Hola mi nombre es {0} y tengo {1} de edad",
                Nombre, Edad);
        }
    }
}
