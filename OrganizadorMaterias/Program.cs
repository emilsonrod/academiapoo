﻿using OrganizadorMaterias.Model;
using OrganizadorMaterias.Tablas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizadorMaterias
{
    class Program
    {
        static void Main(string[] args)
        {
            //Estudiante estudiante1 = new Estudiante() { Nombre = "Juan ANtezana",
            //     CI = "123456cbba", GeneroEst = Genero.masculino};
            //TablaEstudiante tablaEstudiante = new TablaEstudiante();
            //tablaEstudiante.AgregarEstudiante(estudiante1);
            //Estudiante est = tablaEstudiante.BuscarEstudiante("Jua123");
            //est.CI = "987654or";
            //Console.WriteLine(tablaEstudiante.BuscarEstudiante("Jua987").Nombre);

            Materia materia1 = new Materia()
            {
                CodigoMateria = "mat123",
                NombreMateria = "fundamentos"
            };

            TablaMateria tablaMateria = new TablaMateria();
            tablaMateria.AgregarMateria(materia1);
            Materia mat = tablaMateria.BuscarMateria("mat123");
            mat.NombreMateria = "Programacion";
            tablaMateria.EliminarMateria(mat);
        }
    }
}
