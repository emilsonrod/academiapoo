﻿using OrganizadorMaterias.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizadorMaterias.Tablas
{
    public class TablaEstudiante
    {
        List<Estudiante> listaEstudiantes;
        public TablaEstudiante()
        {
            listaEstudiantes = new List<Estudiante>();
        }

        public void AgregarEstudiante(Estudiante estudiante)
        {
            listaEstudiantes.Add(estudiante);
        }
        public Estudiante BuscarEstudiante(string codBuscar)
        {
            return listaEstudiantes.Find(es => es.CodEstudiante.Equals(codBuscar));
        }
        public void EliminarEstudiante(Estudiante estudiante)
        {
            listaEstudiantes.Remove(estudiante);
        }

        ////public void EditarEstudiante(Estudiante estudiante)
        ////{

        ////}
    }
}
