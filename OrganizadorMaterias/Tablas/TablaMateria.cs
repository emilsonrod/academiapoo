﻿using OrganizadorMaterias.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizadorMaterias.Tablas
{
    class TablaMateria
    {
        List<Materia> listaMateria;

        public TablaMateria()
        {
            listaMateria = new List<Materia>();
        }

        public void AgregarMateria(Materia materia)
        {
            listaMateria.Add(materia);
        }
        public Materia BuscarMateria(string codBuscar)
        {
            return listaMateria.Find(mat => mat.CodigoMateria.Equals(codBuscar));
        }
        public void EliminarMateria(Materia materia)
        {
            listaMateria.Remove(materia);
        }

    }
}
