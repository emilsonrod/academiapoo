﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizadorMaterias.Model
{
    public class Materia
    {
        public string NombreMateria { get; set; }
        public string CodigoMateria { get; set; }
    }
}
