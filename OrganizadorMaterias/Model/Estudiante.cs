﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizadorMaterias.Model
{
    

    public class Estudiante
    {
        public string Nombre { get; set; }
        public string CodEstudiante
        {
            get {return  Nombre.Substring(0, 3) + CI.Substring(0, 3); }
        }
        public string CI { get; set; }
        public Genero GeneroEst { get; set; }
        
    }
}
